var app = angular.module('App', ['ngRoute']);
app.config(function($routeProvider){
  $routeProvider
  .when('/', {
  	controller: 'InicioCtrl',
  	templateUrl: 'modules/inicio/inicio.html'
  })
  // Incluir novas rotas para as paginas da aplicacao
  .when('/teste', {
    controller: 'TesteCtrl',
    templateUrl: 'modules/teste/teste.html',
  })
})

var controllers = {};
// Criar os controllers da aplicacao
controllers.InicioCtrl = function($log, $scope, $rootScope) {
	inicioCtrl($log, $scope, $rootScope);
};
controllers.TesteCtrl = function($log, $scope, $rootScope) {
  testeCtrl($log, $scope, $rootScope);
}

app.controller(controllers);